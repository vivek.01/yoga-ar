--------------------------------------------------------------------------------------------------------------------------------------------
Points to rem
1.Check Development Build is unchecked befor uploading final build.

--------------------------------------------------------------------------------------------------------------------------------------------
Project version 2020.3.11f1

--------------------------------------------------------------------------------------------------------------------------------------------
Main Scene Name:-  YOGA

--------------------------------------------------------------------------------------------------------------------------------------------
Android Build Setting changes from default settings :-

1.



--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
PackagesName							Version												Changed to
--------------------------------------------------------------------------------------------------------------------------------------------
Adaptive Performance                    Version 2.1.1 - March 09, 2021                       Removed August 30, 2021
Addressables							Version 1.16.19 - April 15, 2021
Android Logcat							Version 1.2.3 - July 22, 2021
AR Foundation							Version 4.1.7 - April 08, 2021
AR Subsystems							Version 4.1.7 - April 08, 2021
ARCore XR Plugin						Version 4.1.7 - April 08, 2021
Core RP Library							Version 10.5.1 - June 29, 2021
Custom NUnit							Version 1.0.6 - December 07, 2020
Editor Coroutines						Version 1.0.0 - April 23, 2020
DoozyUI
Device Simulator						Version 3.0.1-preview - June 08, 2021
Editor Coroutines						Version 1.0.0 - April 23, 2020
JetBrains Rider Editor					Version 2.0.7 - August 19, 2020
LeanTouch+
Mathematics								Version 1.1.0 - July 11, 2019
Memory Profiler							Version 0.2.10-preview.1 - July 30, 2021
Mobile Notifications					Version 1.3.2 - November 16, 2020
Newtonsoft Json							Version 2.0.0 - April 29, 2020
Profile Analyzer						Version 1.0.3 - August 05, 2020
Quick Search							Version 2.0.2 - July 03, 2020
Scriptable Build Pipeline				Version 1.15.2 - January 27, 2021
Searcher								Version 4.3.2 - March 10, 2021
Shader Graph							Version 10.5.1 - June 29, 2021
Subsystem Registration					Version 1.1.0 - July 27, 2020
Test Framework							Version 1.1.27 - June 16, 2021
TextMeshPro								Version 3.0.6 - April 22, 2021
Timeline								Version 1.4.8 - May 04, 2021
Unity Recorder							Version 2.5.5 - March 17, 2021
Unity UI								Version 1.0.0 - May 25, 2021
Universal RP							Version 10.5.1 - June 29, 2021
Version Control							Version 1.5.7 - April 27, 2021
Visual Studio Code Editor				Version 1.2.3 - October 28, 2020.
Visual Studio Editor					Version 2.0.11 - July 08, 2021
XR Legacy Input Helpers					Version 2.1.7 - December 12, 2020
XR Plugin Management					Version 4.0.1 - February 11, 2021s